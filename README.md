# CHS-holoscanner

## Configuration and System dependencies
- Latest Windows 10 SDK
- Unity LTS Release 2017.4.10f1

## References
- Example App w/ MS Azure - https://docs.microsoft.com/en-us/windows/mixed-reality/mr-azure-305
- Mixed Reality Toolkit
	- https://github.com/Microsoft/MixedRealityToolkit-Unity
	- https://docs.microsoft.com/en-us/windows/mixed-reality/install-the-tools#mixed-reality-toolkit
